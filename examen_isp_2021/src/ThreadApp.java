

public class ThreadApp extends Thread {
    public ThreadApp (String name)
    {
        super(name);
    }
}

    public static void main(String[] args) {
        ThreadApp CThread1 = new ThreadApp("CThread1");
        ThreadApp CThread2 = new ThreadApp("CThread2");

/** in cazul metodei run, fiecare counter numara pe rand
 * in timp ce vasteapta 2 secunde
 *pana sa se printeze urmatoarea valoare
 */

        CThread1.run();
        CThread2.run();
    }

    public void run ()
    {
        for(int i=1; i<=7; i++)
        {
            System.out.println(getName() + " message number  = " + i);
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        System.out.println(getName() + " job finalised.");
    }
